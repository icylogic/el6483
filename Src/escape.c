#include "common.h"

extern __IO bool button_pressed;

// Test whether we are running around an island.
static int8_t route = 0;

static const int32_t LEFT_NO_WALL = 350;
static const int32_t FRONT_NO_WALL = 100;
static const int32_t RIGHT_NO_WALL = 350;
// The distance between the wall and the car. For PID comparing.
static const int32_t WALL_DISTANCE = 90;

static float MIN_RATIO = 0.5;
static float MAX_RATIO = 2;

static void read_data(void);
static void go_straight(void);
static void go_parallel(void);
static void turn_right(void);
static void turn_left(void);
static void turn_back(void);
static void routing(void);
static void follow_wall(void);

static bool all_larger_than(MM data[], MM mm);
static bool all_smaller_than(MM data[], MM mm);

static arm_pid_instance_f32 parallel_pid;
static const float32_t PARALLEL_KP = 10;
static const float32_t PARALLEL_KI = 0;
static const float32_t PARALLEL_KD = 0;

// Sensor data cache
#define DATA_LENGTH 3
static MM front, left, right;
static MM front_data[DATA_LENGTH], left_data[DATA_LENGTH], right_data[DATA_LENGTH];
static size_t front_index = 0, left_index = 0, right_index = 0;
static bool front_error, left_error, right_error;

enum test_status
{
    TEST_1,
    TEST_2,
    TEST_3,
    TEST_4,
    TEST_5,
    TEST_6,
    TEST_7,
    TEST_FINISHED,
    END,
};

enum maze_status
{
    FIND_WALL,
    ESCAPE,
    FINISHED
};

static enum maze_status current_maze_status;

static bool initialized = false;

static const MM FRONT_WALL_FOUND = 200;

static void init()
{
    static int8_t counter = DATA_LENGTH;
    counter--;
    if (counter <= 0)
    {
        initialized = true;
        //TOOD: What if the front space is too small?
        /*
        if (all_smaller_than(front_data, FRONT_WALL_FOUND)) current_maze_status = ESCAPE;
        else current_maze_status = FIND_WALL;
        */
        current_maze_status = ESCAPE;
    }
}

void escape()
{
    read_data();
    static int8_t counter = 20;
    if (!initialized)
    {
        init();
        return;
    }
    // ensure move_by_distance would not be called multiple times
    if (!last_move_finished()) return;
    if (front_error && left_error && right_error) return;
    
    switch (current_maze_status)
    {
    case FIND_WALL:
        if (all_smaller_than(front_data, FRONT_WALL_FOUND))
        {
            move_by_distance(HALF_CIRCLE, 0);
            current_maze_status = ESCAPE;
        }
        else
        {
            go_straight();
        }
        break;
    case ESCAPE:
        //current_maze_status = FINISHED;
        if (counter >= 0)
        {
            stop();
            counter--;
        }
        else
        {
            follow_wall();
            //go_parallel();
        }
        break;
    case FINISHED:
        break;
    }
}

static int8_t turn_left_phase = 0;
static const int32_t ADVANCE_DISTANCE = 80;

static void follow_wall()
{
    if (all_larger_than(left_data, LEFT_NO_WALL))
    {
        if (turn_left_phase == 0)
        {
            move_by_distance(ADVANCE_DISTANCE, ADVANCE_DISTANCE);
        }
        else
        {
            stop();
            HAL_Delay(50);
            move_by_speed((WALL_DISTANCE) * 100 / (WALL_DISTANCE + WHEEL_TRACK_MM), 100);
            HAL_Delay(20);
        }
        turn_left_phase++;
    }
    else
    {
        turn_left_phase = 0;
        if (!all_smaller_than(front_data, FRONT_NO_WALL))
        {
            log("\n\n\n\n\n\n\n\nGO parallel\n");
            go_parallel();
        }
        else if (all_larger_than(right_data, RIGHT_NO_WALL))
        {
            log("\n\n\n\n\n\n\n\n TR \n");
            move_by_distance(HALF_CIRCLE, 0);
        }
        else
        {
            route = 0;
            stop();
            turn_back();
        }
    }
    routing();
}

static void init_parallel_pid()
{
    static bool executed = false;
    if (executed) return;
    parallel_pid.Kp = PARALLEL_KP;
    parallel_pid.Ki = PARALLEL_KI;
    parallel_pid.Kd = PARALLEL_KD;
    arm_pid_init_f32(&parallel_pid, 1);
    executed = true;
}

static enum test_status current_test_status = TEST_7;
static void test()
{
    switch (current_test_status)
    {
    case TEST_1:
        log("TEST1\n\n\n\n");
        move_by_distance(0, HALF_CIRCLE);
        current_test_status = TEST_2;
        break;
    case TEST_2:
        log("TEST2\n\n\n\n");
        move_by_distance(0, HALF_CIRCLE);
        current_test_status = TEST_3;
        break;
    case TEST_3:
        log("TEST3\n\n\n\n");
        move_by_distance(CAR_LENGTH, CAR_LENGTH);
        current_test_status = TEST_4;
        break;
    case TEST_4:
        log("TEST4\n\n\n\n");
        move_by_distance(HALF_CIRCLE, 0);
        current_test_status = TEST_5;
        break;
    case TEST_5:
        log("TEST5\n\n\n\n");
        move_by_distance(HALF_CIRCLE, 0);
        current_test_status = TEST_6;
        break;
    case TEST_6:
        log("TEST6\n\n\n\n");
        move_by_distance(CAR_LENGTH, CAR_LENGTH);
        current_test_status = TEST_7;
        break;
    case TEST_7:
        log("TEST7\n\n\n\n");
        go_parallel();
        //current_test_status = TEST_FINISHED;
        break;
    case TEST_FINISHED:
        log("TEST FINISHED\n\n\n\n");
        stop();
        current_test_status = END;
        break;
    case END:
        break;
    }
}

static bool all_larger_than(MM data[], MM mm)
{ 
    bool result = true;
    for (size_t i = 0; i < DATA_LENGTH; i++)
    {
        result = result && data[i] > mm;
    }
    return result;
}

static bool all_smaller_than(MM data[], MM mm) 
{
    bool result = true;
    for (size_t i = 0; i < DATA_LENGTH; i++)
    {
        result = result && data[i] < mm;
    }
    return result;

}
static bool avg_larger_than(MM data1[], MM data2[]) 
{ 
    MM sum1 = 0, sum2 = 0;
    for (size_t i = 0; i < DATA_LENGTH; i++)
    {
        sum1 += data1[i];
        sum2 += data2[i];
    }
    return sum1 > sum2;
}

static void add_data(MM data[], MM mm, size_t *index)
{
    data[*index] = mm;
    (*index)++;
    if (*index == DATA_LENGTH) *index = 0;
}

void read_data()
{
    left = get_distance_left();
    left_error = left == IMMESAURABLE_DISTANCE;
    if (!left_error) add_data(left_data, left, &left_index);
    right = get_distance_right();
    right_error = right == IMMESAURABLE_DISTANCE;
    if (!right_error) add_data(right_data, right, &right_index);
    front = get_distance_front();
    front_error = front == IMMESAURABLE_DISTANCE;
    if (!front_error) add_data(front_data, front, &front_index);
}

void routing()
{
    if (route >= 4)
    {
        turn_back();
        current_maze_status = FIND_WALL;
        route = 0;
    }
}

void turn_left()
{
    move_by_distance(0, HALF_CIRCLE);
}

void turn_right()
{
    move_by_distance(HALF_CIRCLE, 0);
}

void go_straight()
{
    move_by_speed(MIN_CW_SPEED, MIN_CW_SPEED);
}


void go_parallel()
{
    if (left_error) return;
    init_parallel_pid();
    int32_t error = left - WALL_DISTANCE;

    float32_t output = arm_pid_f32(&parallel_pid, error);
    log("  error: %i, pid output: %i, ", error, (int) output);
    float left_over_right = 1 - output / 100;

    stop();
    measure_distance();
    read_data();
    measure_distance();
    read_data();
    measure_distance();
    read_data();
    measure_distance();
    read_data();
    HAL_Delay(50);

    if (left_over_right == 1)
    {
        move_by_speed(MIN_CW_SPEED, MIN_CW_SPEED);
    }
    else if (left_over_right < 1)
    {
        if (left_over_right < MIN_RATIO) left_over_right = MIN_RATIO;
        log("  left_over_right %i\n", (int)(left_over_right*100));
        move_by_speed(MIN_CW_SPEED, (MM) (MIN_CW_SPEED / left_over_right));
    }
    else
    {
        if (left_over_right > MAX_RATIO) left_over_right = MAX_RATIO;
        log("  left_over_right %i\n", (int)(left_over_right*100));
        move_by_speed((MM)(MIN_CW_SPEED / left_over_right), MIN_CW_SPEED);
    }
    HAL_Delay(20);
}

void turn_back()
{
    //When turning back, check the space left for turning.
    if (avg_larger_than(left_data, right_data))
    {
        move_by_distance(-HALF_CIRCLE, HALF_CIRCLE);
    }
    else
    {
        move_by_distance(HALF_CIRCLE, -HALF_CIRCLE);
    }
}
