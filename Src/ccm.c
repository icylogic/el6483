// this file will be placed in CCM RAM
// symbols in this file should be used frequently, like IRQ handlers

#include "common.h"
#include <sys/param.h>


// Distance sensors
TIM_HandleTypeDef DistanceTimerHandle;
__IO struct SensorsStatus Sensors = {.front = 0, .left = 0, .right = 0};
enum ultrasonic_status
{
    TRIGGERED,
    ECHOING,
    FINISHED,
    TIMEOUT
};
static enum ultrasonic_status left_status = TRIGGERED, right_status = TRIGGERED, front_status = TRIGGERED;

static __IO int32_t __attribute__((aligned(4))) elapsed = 0;
/**
 * \brief Total interval (60ms) recommanded in datasheet.
 */
static const int32_t ELAPSED_STEP = DISTANCE_TIMER_PERIOD * 1000000 / DISTANCE_TIMER_FREQUENCY;
static const float UltrasonicFactor = (float) 1.7;
void measure_distance();

static uint32_t pulse_width(uint32_t percentage)
{
    if (percentage >= 100) return PWM_PERIOD;
    if (percentage == 0) return 0;
    return PWM_PERIOD * percentage / 100;
}


// Motors
TIM_OC_InitTypeDef PWMTimerOCConfig;
TIM_HandleTypeDef PWMTimerHandle;
__IO struct WheelsStatus moved_since_last_command = {.left = 0, .right = 0};
MM get_moved_since_last_command_left() { return moved_since_last_command.left; }
MM get_moved_since_last_command_right() { return moved_since_last_command.right; }
// a global movement counter that never clear
__IO struct WheelsStatus moved = {.left = 0, .right = 0};
MM get_moved_left() { return moved.left; }
MM get_moved_right() { return moved.right; }
// um/ms
__IO struct WheelsStatus Speed = {.left = 0, .right = 0};
MM get_speed_left() { return Speed.left; }
MM get_speed_right() { return Speed.right; }
static void update_pwm();
static bool is_straight = false;
static int8_t stop_counter = 0;

static int8_t left_not_moved = 0;
static int8_t right_not_moved = 0;

enum MoveMode
{
    SPEED,
    DISTANCE
};

static enum MoveMode move_mode = SPEED;
static void control_by_speed();
static void control_by_distance();


/**
 * \brief Real movement since last move command
 */
static struct WheelsStatus last_move_command = {.left = 0, .right = 0};

static MM last_move_max;
// -100 ~ 100
static struct MotorOption pwm_ratio = {.left = 0, .right = 0};
static void set_pwm(uint32_t channel, bool start, uint32_t pulse);
static void motor_control();


static const int32_t MIN_LOOP_DURATION = 60;
extern __IO bool button_pressed;

void loop()
{
    // TODO: State Machine
    uint32_t last_measure = 0;
    while (true)
    {
        uint32_t start = HAL_GetTick();

        uint32_t since_last_measure = time_elapsed(last_measure);
        if (since_last_measure < DISTANCE_SENSOR_MIN_PERIOD)
        {
            HAL_Delay(DISTANCE_SENSOR_MIN_PERIOD - since_last_measure);
        }
        last_measure = HAL_GetTick();
        measure_distance();

        if (last_move_command.left != 0 || last_move_command.right != 0)
        {
            motor_control();
        }

        if (stop_counter == 0)
        {
            escape();
        }

        // Get duty cycle
        //interval = TimeElapsed(start);
        //log("  Moved: %i mm, %i mm\n", Moved.left, Moved.right);
        //log("Duty: %u ms\n", time_elapsed(start));
        //HAL_Delay(MIN_LOOP_DURATION);
    }
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim)
{
    if (htim->Instance == DistanceTimerHandle.Instance)
    {
        elapsed++;
    }
}

static void switch_timer(uint8_t flag)
{
    if (flag)
    {
        elapsed = 0;
        if (HAL_TIM_Base_Start_IT(&DistanceTimerHandle) != HAL_OK)
        {
            /* Starting Error */
            Error_Handler();
        }
    }
    else
    {
        if (HAL_TIM_Base_Stop(&DistanceTimerHandle) != HAL_OK)
        {
            /* Starting Error */
            Error_Handler();
        }
    }
}

static inline void check_sensor_echo(enum ultrasonic_status* status, int32_t* timer, GPIO_TypeDef* port, uint16_t pin)
{
    switch (*status)
    {
    case TRIGGERED:
        if (HAL_GPIO_ReadPin(port, pin) == GPIO_PIN_SET)
        {
            *timer = elapsed;
            *status = ECHOING;
        }
        break;
    case ECHOING:
        if (HAL_GPIO_ReadPin(port, pin) == GPIO_PIN_RESET)
        {
            *timer = elapsed - (*timer);
            *status = FINISHED;
        }
        break;
    case FINISHED:
    case TIMEOUT:
        break;
    }
}


void measure_distance()
{
    log("  Measure Distance: ");

    //TODO: use seperate hardware timer and trigger (with filter)
    int32_t left_elapsed = 0, right_elapsed = 0, front_elapsed = 0;
    HAL_GPIO_WritePin(DISTANCE_LEFT_TRIG_PORT, DISTANCE_LEFT_TRIG_PIN, GPIO_PIN_SET);
    HAL_GPIO_WritePin(DISTANCE_FRONT_TRIG_PORT, DISTANCE_FRONT_TRIG_PIN, GPIO_PIN_SET);
    HAL_GPIO_WritePin(DISTANCE_RIGHT_TRIG_PORT, DISTANCE_RIGHT_TRIG_PIN, GPIO_PIN_SET);
    switch_timer(1);
    while (elapsed * ELAPSED_STEP < 10)
    {
    }
    switch_timer(0);
    HAL_GPIO_WritePin(DISTANCE_LEFT_TRIG_PORT, DISTANCE_LEFT_TRIG_PIN, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(DISTANCE_FRONT_TRIG_PORT, DISTANCE_FRONT_TRIG_PIN, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(DISTANCE_RIGHT_TRIG_PORT, DISTANCE_RIGHT_TRIG_PIN, GPIO_PIN_RESET);

    left_status = TRIGGERED;
    right_status = TRIGGERED;
    front_status = TRIGGERED;

    switch_timer(1);
    do
    {
        check_sensor_echo(&left_status, &left_elapsed, DISTANCE_LEFT_ECHO_PORT, DISTANCE_LEFT_ECHO_PIN);
        check_sensor_echo(&front_status, &front_elapsed, DISTANCE_FRONT_ECHO_PORT, DISTANCE_FRONT_ECHO_PIN);
        check_sensor_echo(&right_status, &right_elapsed, DISTANCE_RIGHT_ECHO_PORT, DISTANCE_RIGHT_ECHO_PIN);
        if (elapsed * ELAPSED_STEP > DISTANCE_SENSOR_MIN_PERIOD * 1000)
        {
            //HAL_GPIO_WritePin(DISTANCE_RIGHT_TRIG_PORT, DISTANCE_RIGHT_TRIG_PIN, GPIO_PIN_SET);
            if (left_status != FINISHED) 
            {
                left_status = TIMEOUT;
                Sensors.left = IMMESAURABLE_DISTANCE;
            }
            if (front_status != FINISHED) 
            {
                front_status = TIMEOUT;
                Sensors.front = IMMESAURABLE_DISTANCE;
            }
            if (right_status != FINISHED)
            {
                right_status = TIMEOUT;
                Sensors.right = IMMESAURABLE_DISTANCE;
            }
            break;
        }
    }
    while (left_status != FINISHED || front_status != FINISHED || right_status != FINISHED);
    switch_timer(0);

    float distance;
    if (left_status == FINISHED)
    {
        distance = left_elapsed / 2 * UltrasonicFactor;
        Sensors.left = (MM) distance;
    }
    if (front_status == FINISHED)
    {
        distance = front_elapsed / 2 * UltrasonicFactor;
        Sensors.front = (MM) distance;
    }
    if (right_status == FINISHED)
    {
        distance = right_elapsed / 2 * UltrasonicFactor;
        Sensors.right = (MM) distance;
    }
    log("Left: %d mm, Front: %d mm, Right: %d mm\n", Sensors.left, Sensors.front, Sensors.right);
}

MM get_distance_front() { return Sensors.front; }
MM get_distance_left() { return Sensors.left; }
MM get_distance_right() { return Sensors.right; }


/**
  * @brief EXTI line detection callbacks
  * @param GPIO_Pin Specifies the pins connected EXTI line
  * @retval None
  */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    static uint32_t last_left = 0;
    static uint32_t last_right = 0;
    int32_t since_last;
    MM Movement;
    switch (GPIO_Pin)
    {
    case STEP_LEFT_PIN:
        since_last = (int32_t) time_elapsed(last_left);
        if (since_last < STEP_SENSOR_MIN_PERIOD) return;
        left_not_moved = 0;
        last_left = HAL_GetTick();
        // Revolutions for wheel 1
        if (pwm_ratio.left < 0)
        {
            Movement = -WHEEL_STEP_MM;
            Speed.left = -WHEEL_STEP_UM / since_last;
        }
        else if (pwm_ratio.left > 0)
        {
            Movement = WHEEL_STEP_MM;
            Speed.left = WHEEL_STEP_UM / since_last;
        }
        else
        {
            // Try to infer direction if last move command for a wheel is 0 but the real movement is not zero
            // It may be caused by the movement of another wheel
            Movement = (pwm_ratio.right < 0) ? -WHEEL_STEP_MM : WHEEL_STEP_MM;
        }
        moved_since_last_command.left += Movement;
        moved.left += Movement;
        log("  left: %i\n", moved.left);
        break;
    case STEP_RIGHT_PIN:
        //log("   ??\n");
        since_last = (int32_t) time_elapsed(last_right);
        if (since_last < STEP_SENSOR_MIN_PERIOD) return;
        right_not_moved = 0;
        last_right = HAL_GetTick();
        // Revolutions for wheel 2
        if (pwm_ratio.right < 0)
        {
            Movement = -WHEEL_STEP_MM;
            Speed.right = -WHEEL_STEP_UM / since_last;
        }
        else if (pwm_ratio.right > 0)
        {
            Movement = WHEEL_STEP_MM;
            Speed.right = WHEEL_STEP_UM / since_last;
        }
        else
        {
            Movement = (pwm_ratio.left < 0) ? -WHEEL_STEP_MM : WHEEL_STEP_MM;
        }
        moved_since_last_command.right += Movement;
        moved.right += Movement;
        log("  right: %i\n", moved.right);
        break;
    case USER_BUTTON_PIN:
        button_pressed = true;
        break;
    default:
        break;
    }
}

inline uint32_t interval(uint32_t from, uint32_t to)
{
    return to - from;
}

inline uint32_t time_elapsed(uint32_t when)
{
    return interval(when, HAL_GetTick());
}

// Motor Control

static void set_pwm(uint32_t channel, bool start, uint32_t pulse)
{
    if (start)
    {
        /* Set the pulse value for channel */
        PWMTimerOCConfig.Pulse = pulse;
        if (HAL_TIM_PWM_ConfigChannel(&PWMTimerHandle, &PWMTimerOCConfig, channel) != HAL_OK)
        {
            /* Configuration Error */
            Error_Handler();
        }
        /* Start channel */
        if (HAL_TIM_PWM_Start(&PWMTimerHandle, channel) != HAL_OK)
        {
            /* PWM Generation Error */
            Error_Handler();
        }
    }
    else
    {
        if (HAL_TIM_PWM_Stop(&PWMTimerHandle, channel) != HAL_OK)
        {
            /* PWM Generation Error */
            Error_Handler();
        }
    }
}

struct WheelsStatus get_last_move_command()
{
    return last_move_command;
}


static void update_pwm()
{
    bool isClockwise;
    log("ratio: %i, %i\n", pwm_ratio.left, pwm_ratio.right);
    if (pwm_ratio.left == 0)
    {
        set_pwm(PWM_LEFT_CW_CHANNEL, false, 0);
        set_pwm(PWM_LEFT_CCW_CHANNEL, false, 0);
    }
    else
    {
        isClockwise = pwm_ratio.left > 0;
        uint32_t pulse = pulse_width((uint32_t) abs(pwm_ratio.left));
        log("  pulse left: %i\n", pulse);
        set_pwm(PWM_LEFT_CW_CHANNEL, isClockwise, pulse);
        set_pwm(PWM_LEFT_CCW_CHANNEL, !isClockwise, pulse);
    }
    if (pwm_ratio.right == 0)
    {
        set_pwm(PWM_RIGHT_CW_CHANNEL, false, 0);
        set_pwm(PWM_RIGHT_CCW_CHANNEL, false, 0);
    }
    else
    {
        isClockwise = pwm_ratio.right > 0;
        uint32_t pulse = pulse_width((uint32_t) abs(pwm_ratio.right));
        log("  pulse right: %i\n", pulse);
        set_pwm(PWM_RIGHT_CW_CHANNEL, isClockwise, pulse);
        set_pwm(PWM_RIGHT_CCW_CHANNEL, !isClockwise, pulse);
    }
}


static int32_t left_ratio = 0;
static int32_t right_ratio = 0;

/*
The input must be either
left == 0,
right == 0,
left == right
 */

static void move(MM left, MM right)
{
    last_move_command.left = left;
    last_move_command.right = right;
    moved_since_last_command.left = 0;
    moved_since_last_command.right = 0;
    last_move_max = MAX(abs(left), abs(right));

    // if a motor is stopped or running in the opposite direction,
    // we need to give it an initial pulse large enough.
    if (left == 0)
    {
        pwm_ratio.left = 0;
        left_ratio = 0;
    }
    else
    {
        if (left < 0 && pwm_ratio.left >= 0)
        {
            pwm_ratio.left = MOTOR_MIN_INIT_CCW_RATIO;
        }
        else if (left > 0 && pwm_ratio.left <= 0)
        {
            pwm_ratio.left = MOTOR_MIN_INIT_CW_RATIO;
        }
    }

    if (right == 0)
    {
        pwm_ratio.right = 0;
    }
    else
    {
        if (right < 0 && pwm_ratio.right >= 0)
        {
            pwm_ratio.right = MOTOR_MIN_INIT_CCW_RATIO;
        }
        else if (right > 0 && pwm_ratio.right <= 0)
        {
            pwm_ratio.right = MOTOR_MIN_INIT_CW_RATIO;
        }
    }
    update_pwm();
    HAL_Delay(10);
    motor_control();
}

static bool is_distance_finished = true;

// with help of encoders
void move_by_distance(MM left, MM right)
{
    move_mode = DISTANCE;
    //arm_pid_init_f32(&left_motor_pid, 1);
    //arm_pid_init_f32(&right_motor_pid, 1);
    is_straight = left == right ? true : false;
    log("    Move Command: %i, %i\n", left, right);
    is_distance_finished = false;
    move(left, right);
}

void move_by_speed(MM left, MM right)
{
    move_mode = SPEED;
    log(" speed: left %i %i", left, right);
    move(left, right);
}

void stop()
{
    log("\n\n\n\n*******STOP*********\n\n\n\n");
    last_move_command.left = 0;
    last_move_command.right = 0;
    moved_since_last_command.left = 0;
    moved_since_last_command.right = 0;
    pwm_ratio.left = 0;
    pwm_ratio.right = 0;
    is_distance_finished = true;

    update_pwm();
}

static const int8_t MAX_NOT_MOVED = 20;

static void motor_control()
{
    switch (move_mode)
    {
    case SPEED:
        control_by_speed();
        break;
    case DISTANCE:
        control_by_distance();
        break;
    }
    left_not_moved = 0;
}

static void control_by_speed()
{
    //TODO: Use PID control
    if (last_move_max == 0) return;
    pwm_ratio.left = last_move_command.left;
    pwm_ratio.right = last_move_command.right;
    update_pwm();
}

bool last_move_finished()
{
    switch (move_mode)
    {
    case SPEED:
        return true;
    case DISTANCE:
        {
            if (is_distance_finished) return true;
            MM error_left = moved_since_last_command.left - last_move_command.left;
            MM error_right = moved_since_last_command.right - last_move_command.right;
            bool left_finished = last_move_command.left == 0 ||
                ((last_move_command.left > 0) == (error_left > 0)) ||
                abs(error_left) <= 0; // WHEEL_STEP_MM * 1;
            bool right_finished = last_move_command.right == 0 ||
                ((last_move_command.right > 0) != (error_right > 0)) ||
                abs(error_right) <= 0; //WHEEL_STEP_MM * 1;
            return left_finished && right_finished;
        }
    }
}

static void control_by_distance()
{
    //TODO: Use complementary signal for motors

    //TODO: Handle the situation that the car moved beyond destination
    MM error_left = last_move_command.left - moved_since_last_command.left;
    if ((error_left <= 0) != (last_move_command.left <= 0)) error_left = 0;
    MM error_right = last_move_command.right - moved_since_last_command.right;
    if ((error_right <= 0) != (last_move_command.right <= 0)) error_right = 0;

    if (error_left == 0 || last_move_command.left == 0)
    {
        error_left = 0;
    }

    if (error_right == 0 || last_move_command.right == 0)
    {
        error_right = 0;
    }

    if (error_left == 0 && error_right == 0)
    {
        stop();
        return;
    }

    log("  error %i, %i\n", error_left, error_right);

    if (error_left < -MOVE_ERROR_THRESHOLD)
    {
        pwm_ratio.left = MIN_CCW_SPEED;
    }
    else if (error_left > MOVE_ERROR_THRESHOLD)
    {
        pwm_ratio.left = MIN_CW_SPEED;
    }
    else
    {
        pwm_ratio.left = 0;
        if (is_straight) pwm_ratio.right = 0;
    }

    if (error_right < -MOVE_ERROR_THRESHOLD)
    {
        pwm_ratio.right = MIN_CCW_SPEED;
    }
    else if (error_right > MOVE_ERROR_THRESHOLD)
    {
        pwm_ratio.right = MIN_CW_SPEED;
    }
    else
    {
        pwm_ratio.right = 0;
        if (is_straight) pwm_ratio.left = 0;
    }
    update_pwm();
}

// IRQ Handlers
void SysTick_Handler(void)
{
    HAL_IncTick();
    HAL_SYSTICK_IRQHandler();
}

void EXTI2_TSC_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);
}

void EXTI0_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
}

void EXTI3_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_3);
}

void TIM4_IRQHandler(void)
{
    HAL_TIM_IRQHandler(&DistanceTimerHandle);
}
